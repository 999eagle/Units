using System;
using System.Numerics;

namespace Units
{
	internal class Util
	{
		private static readonly BigInteger zettaConversion = BigInteger.Pow(10, 21);
		private static readonly BigInteger yottaConversion = BigInteger.Pow(10, 24);

		public static int GCD(int a, int b)
		{
			while (b != 0)
			{
				var t = b;
				b = a % b;
				a = t;
			}

			return a;
		}

		public static long GCD(long a, long b)
		{
			while (b != 0)
			{
				var t = b;
				b = a % b;
				a = t;
			}

			return a;
		}

		public static int ShiftAndWrap(int value, int shift)
		{
			shift &= 0x1F;
			var number = BitConverter.ToUInt32(BitConverter.GetBytes(value), 0);
			var wrapped = number >> (32 - shift);
			return BitConverter.ToInt32(BitConverter.GetBytes((number << shift) | wrapped), 0);
		}

		public static Ratio GetPrefixRatio(char prefix)
		{
			return prefix switch
			{
				'h' => new Ratio(1, 100),
				'k' => new Ratio(1, 1000),
				'M' => new Ratio(1, 1000_000),
				'G' => new Ratio(1, 1000_000_000),
				'T' => new Ratio(1, 1000_000_000_000),
				'P' => new Ratio(1, 1000_000_000_000_000),
				'E' => new Ratio(1, 1000_000_000_000_000_000),
				'Z' => new Ratio(1, zettaConversion),
				'Y' => new Ratio(1, yottaConversion),
				'c' => new Ratio(100),
				'm' => new Ratio(1000),
				'μ' => new Ratio(1000_000),
				'n' => new Ratio(1000_000_000),
				'p' => new Ratio(1000_000_000_000),
				'f' => new Ratio(1000_000_000_000_000),
				'a' => new Ratio(1000_000_000_000_000_000),
				'z' => new Ratio(zettaConversion),
				'y' => new Ratio(yottaConversion),
				_ => new Ratio()
			};
		}
	}
}
